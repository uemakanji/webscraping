from django.shortcuts import render
from django.utils import timezone
from bs4 import BeautifulSoup
import requests


def weather_list(request):
    tj = tenki_jp()
    jgj = jma_go_jp()
    #wn = weather_news()
    #weather_sites = [tj, jgj, wn]
    weather_sites = [tj, jgj]

    return render(request, 'weather/weather_list.html',
                {'weather_sites': weather_sites})


def tenki_jp():
    url = 'https://tenki.jp/forecast/3/16/4410/13101/'
    req = requests.get(url)
    bs_obj = BeautifulSoup(req.content, 'html.parser')

    today = bs_obj.find(class_='today-weather')
    today_weather = today.find('p', class_='weather-telop').string
    today_high_temp = today.find('dd', class_='high-temp temp').find(class_='value').string + "度"
    today_low_temp = today.find('dd', class_='low-temp temp').find(class_='value').string + "度"
    tomorrow = bs_obj.find(class_='tomorrow-weather')
    tomorrow_weather = tomorrow.find('p', class_='weather-telop').string
    tomorrow_high_temp = tomorrow.find('dd', class_='high-temp temp').find(class_='value').string + "度"
    tomorrow_low_temp = tomorrow.find('dd', class_='low-temp temp').find(class_='value').string + "度"
    
    tj = {'web_site': '気象協会',
        'today_weather': today_weather,
        'today_high_temp': today_high_temp,
        'today_low_temp': today_low_temp,
        'tomorrow_weather': tomorrow_weather,
        'tomorrow_high_temp': tomorrow_high_temp,
        'tomorrow_low_temp': tomorrow_low_temp,
        'url': url}
    
    return tj


def jma_go_jp():
    url = 'https://www.jma.go.jp/jp/yoho/319.html'
    req = requests.get(url)
    bs_obj = BeautifulSoup(req.content, 'html.parser')
    weather = bs_obj.find_all('th', class_='weather')
    temp = bs_obj.find_all('td', class_='temp')
    temp_max = temp[0].find(class_='max')
    temp_min = temp[0].find(class_='min')

    today_weather = weather[0].img['title']
    today_high_temp = "--度" if temp_max is None else temp_max.string
    today_low_temp = "--度" if (temp_min is None) or (temp_min.string is None) else temp_min.string
    tomorrow_weather = weather[1].img['title']
    tomorrow_high_temp = temp[1].find(class_='max').string
    tomorrow_low_temp = temp[1].find(class_='min').string

    jgj = {'web_site': '気象庁',
        'today_weather': today_weather,
        'today_high_temp': today_high_temp,
        'today_low_temp': today_low_temp,
        'tomorrow_weather': tomorrow_weather,
        'tomorrow_high_temp': tomorrow_high_temp,
        'tomorrow_low_temp': tomorrow_low_temp,
        'url': url}
    
    return jgj

#ウェザーニュースの取得が難しい
"""
def weather_news():
    url = 'https://www.google.co.jp/search?ei=yCXLW_yIOpPQ8wXV85nYCg&q=%E5%A4%A9%E6%B0%97%E4%BA%88%E5%A0%B1+%E6%9D%B1%E4%BA%AC&oq=%E5%A4%A9%E6%B0%97%E4%BA%88%E5%A0%B1+&gs_l=psy-ab.3.0.0l6j0i131k1j0.11444.23332.0.24620.29.24.5.0.0.0.116.1729.19j2.23.0....0...1c.1j4.64.psy-ab..3.24.1577.0..0i4k1j0i67k1j0i10i67k1j0i131i67k1.74.w830lru-uAU'
    req = requests.get(url)
    bs_obj = BeautifulSoup(req.content, 'html.parser')
    weather = bs_obj.find_all('img')
    temp = bs_obj.find_all('span', class_='wob_t')
    
    today_weather = weather[1]['title']
    today_high_temp = temp[2].string[:-2] + '度'
    today_low_temp = temp[3].string[:-2] + '度'
    tomorrow_weather = weather[2]['title']
    tomorrow_high_temp = temp[4].string[:-2] + '度'
    tomorrow_low_temp = temp[5].string[:-2] + '度'

    wn = {'web_site': 'ウェザーニュース',
        'today_weather': today_weather,
        'today_high_temp': today_high_temp,
        'today_low_temp': today_low_temp,
        'tomorrow_weather': tomorrow_weather,
        'tomorrow_high_temp': tomorrow_high_temp,
        'tomorrow_low_temp': tomorrow_low_temp,
        'url': 'https://weathernews.jp/s/forecast/detail.fcgi?area=Tokyo'}

    return wn
"""