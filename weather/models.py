from django.db import models


class Weather(models.Model):
    weather_site = models.CharField('天気サイト', max_length=255)
    tenki = models.CharField('天気', max_length=255)
    weather_date = models.CharField('天気日時', max_length=255)
    high_temperature = models.IntegerField('最高気温')
    low_temperature = models.IntegerField('最低気温')
    rainy_percent_0006 = models.IntegerField('降水確率_0006')
    rainy_percent_0612 = models.IntegerField('降水確率_0612')
    rainy_percent_1218 = models.IntegerField('降水確率_1218')
    rainy_percent_1824 = models.IntegerField('降水確率_1824')
    